#include <Adafruit_NeoPixel.h>

//Arduino Uno
//#define PIN 6
//#define ANI 0

//AtTiny85
#define PIN 1
#define ANI 1


#define colors 6

#define rows 30

int ppc; //Pixels Per Color

int ColorSelect[][3] = {  {255, 0, 0}, {255, 127, 0}, {255, 255, 0}, {28, 172, 120}, {0, 0, 255}, {159, 0, 255} };
uint32_t CS[colors];

Adafruit_NeoPixel strip = Adafruit_NeoPixel(rows, PIN, NEO_GRB + NEO_KHZ800);

const int sampleWindow = 35; // Sample window width in mS (50 mS = 20Hz)

unsigned int sample;

unsigned int pixel;

unsigned int pe;

double v;

uint32_t peak;

uint32_t peakColor;

int prevPixel = 1;

unsigned long prevMillis;

unsigned long dropMillis;

//unsigned long resetMillis;

int previousReset;

int resetCounter;

//int currentSum;

int pixelMax;
int pixelMin = 1024;
int pixelMaxPrev;
int pixelMinPrev;
//int pMin;
//int pMax;

//int currentMax;
//int previousMin;

//int changeVar = 5;
int maxChange;
int minChange;

int recRes = 0;

int vMult = 100;

int vAdd = 1;

void setup()
{

  //Serial.begin(9600);
  for (int i = 0; i < 6; i++) {
    CS[i] = strip.Color(ColorSelect[i][0], ColorSelect[i][1], ColorSelect[i][2]);
    //Serial.println(CS[i]);
  }

  strip.begin();
  strip.show();
}


void loop()
{
  unsigned long startMillis = millis(); // Start of sample window



  unsigned int peakToPeak = 0;   // peak-to-peak level

  unsigned int signalMax = 0;
  unsigned int signalMin = 1024;

  // collect data for 50 mS
  while (millis() - startMillis < sampleWindow)
  {
    sample = analogRead(ANI);
    if (sample < 1024)  // toss out spurious readings
    {
      if (sample > signalMax)
      {
        signalMax = sample;  // save just the max levels
      }
      else if (sample < signalMin)
      {
        signalMin = sample;  // save just the min levels
      }
    }
  }
  peakToPeak = signalMax - signalMin;  // max - min = peak-peak amplitude
  double volts = (peakToPeak * 3.3) / 255;  // convert to volts


  volts *= vAdd;

  v = volts * vMult;
  //Serial.println(v);

  pixel = (int) v;

  if (pixel > pixelMax || pixelMin > pixel) {
    if (pixel > pixelMax) {
      if (recRes == 0) {
        pixelMaxPrev = pixel;
      }
      else {
        pixelMaxPrev = pixelMax;
      }
      pixelMax = pixel;

      maxChange = pixelMax - pixelMaxPrev;
    }
    if (pixelMin > pixel) {
      if (recRes == 0) {
        pixelMinPrev = pixel;
      }
      else {
        pixelMinPrev = pixelMin;
      }
      pixelMin = pixel;

      minChange = pixelMinPrev - pixelMin;
    }

  }

  //recRes++;
  strip.clear();
  if (pixelMin < pixelMax && pixelMin != pixelMax) {
    scaleFunction();
  }

  //if (recRes > 2) {
  //}

}

void resetFunction() {

  int oneColor = rows / colors + 1;
  int oneColorTop = rows - oneColor;
  int halfRows = rows / 2 - 1;


  if (prevPixel < 2) {
    resetCounter++;
  }
  if (prevPixel < oneColor) {
    resetCounter++;
  }
  if (prevPixel == halfRows) {
    resetCounter--;

  }

  if (pe > oneColorTop) {
    resetCounter++;
  }

  if (pe == rows - 1) {
    resetCounter++;
  }

  if (pe > halfRows && pe < rows - 1) {
    resetCounter++;
  }
  if (prevPixel == rows - 1) {
    resetCounter--;
  }


  if (pe > halfRows) {
    resetCounter--;
  }

  if (prevPixel < halfRows) {
    resetCounter++;
  }

  if (pe > 0) {
    resetCounter--;
  }

  if (resetCounter > 50) {
    //pixelMin = pixelMax;
    pixelMax = pixelMin;
    resetCounter = 0;
    previousReset++;
  }

  if (resetCounter < -50) {
    previousReset--;
    resetCounter = 0;
    pixelMin = pixelMax;
    if (vAdd > 1) {
      vAdd -= 1;
    }
  }

  if (previousReset > 5) {
    vAdd += 1;
    previousReset = 0;
    //Serial.println("Voltage Multiple Changed");
  }



  //Serial.println(resetCounter);

}

/*void resetFunction_change() {
  if (minChange > changeVar || maxChange > changeVar) {
    pixelMin = pixelMax;
    pixelMax = 0;

    recRes = 0;
  }

}*/

void scaleFunction() {

  resetFunction();

  int scaleTop;
  int scaleBottom;

  //if(pixelMin < pixelMax) {
  scaleTop = pixelMax;
  scaleBottom = pixelMin;
  //}
  int spixel;
  int ColorNum = rows / colors;
  int currentAverage;
  unsigned long startMillis = millis();


  spixel = map(pixel, scaleBottom, scaleTop, 0, rows - 1);



  for (int i = 0; i < spixel; i++) {

    int p = i;
    if (p > spixel - ColorNum) {
      strip.setPixelColor(i, CS[5]);
    }
    else if (p > spixel - ColorNum * 2 && p <= spixel - ColorNum * 1) {
      strip.setPixelColor(i, CS[4]);
    }
    else if (p > spixel - ColorNum * 3 && p <= spixel - ColorNum * 2) {
      strip.setPixelColor(i, CS[3]);
    }
    else if (p > spixel - ColorNum * 4 && p <= spixel - ColorNum * 3) {
      strip.setPixelColor(i, CS[2]);
    }
    else if (p > spixel - ColorNum * 5 && p <= pixel - ColorNum * 4) {
      strip.setPixelColor(i, CS[1]);
    }
    else {
      strip.setPixelColor(i, CS[0]);
    }
  }


  strip.show();



  peakColor = strip.getPixelColor(0);

  if (prevPixel < spixel) {
    prevPixel = spixel + 1;
    peak = peakColor;
    prevMillis = startMillis;
    strip.setPixelColor(prevPixel, peak);
  }
  else {

    if (startMillis - prevMillis < 100) {
      strip.setPixelColor(prevPixel, peak);
    }
    else {
      if (startMillis - dropMillis > 50) {
        prevPixel -= 1;
        strip.setPixelColor(prevPixel, peak);
        dropMillis = startMillis;
      }
      else {
        strip.setPixelColor(prevPixel, peak);
      }
    }
  }

  strip.show();

  pe = spixel;

}
